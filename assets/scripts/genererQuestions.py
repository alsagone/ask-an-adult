import random
import datetime
import json

lorem_70 = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent nam."
lorem_140 = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. In et lectus eu elit accumsan elementum. Vestibulum convallis blandit nisl, sit eu."
lorem_280 = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut vitae consectetur orci, ut hendrerit leo. Quisque vestibulum eros a justo tincidunt, ac sollicitudin orci rutrum. Aliquam commodo mollis magna eget suscipit. Cras dolor lacus, commodo eget ligula nec, porttitor accumsan."
lorem_arr = [lorem_70, lorem_140, lorem_280]

sujets_arr = [
    'Achats',
    'Argent',
    'Cuisine',
    'Ménage',
    'Santé',
    'Travail',
    'Autre',
]


class Question:
    def __init__(self, titre: str, texte: str, id_question: int, id_auteur: int, timestamp: int, sujet: str, upvotes: int, downvotes: int, nb_commentaires: int):
        self.titre = titre
        self.texte = texte
        self.id_question = id_question
        self.id_auteur = id_auteur
        self.timestamp = timestamp
        self.sujet = sujet
        self.upvotes = upvotes
        self.downvotes = downvotes
        self.nb_commentaires = nb_commentaires

    def to_dict(self):
        d = {}
        d["titre"] = self.titre
        d["texte"] = self.texte
        d["id_question"] = self.id_question
        d["id_auteur"] = self.id_auteur
        d["timestamp"] = self.timestamp
        d["sujet"] = self.sujet
        d["upvotes"] = self.upvotes
        d["downvotes"] = self.downvotes
        d["nb_commentaires"] = self.nb_commentaires
        return d


def get_random_date() -> datetime:
    offsetHours = (-1) * random.randint(0, 24)
    offsetMinutes = (-1) * random.randint(0, 59)
    return datetime.datetime.today() + datetime.timedelta(hours=offsetHours, minutes=offsetMinutes)


def get_timestamp(d: datetime) -> int:
    return int(datetime.datetime.timestamp(d))


def generer_question(id_question: int) -> Question:
    texte = random.choice(lorem_arr)
    titre = texte[:80]
    sujet = random.choice(sujets_arr)
    timestamp = get_timestamp(get_random_date())
    upvotes = random.randint(0, 100)
    downvotes = random.randint(0, upvotes)
    nb_commentaires = random.randint(0, 100)
    return Question(titre, texte, id_question, 1, timestamp, sujet, upvotes, downvotes, nb_commentaires)


def stocker_questions():
    liste_questions = [generer_question(i).to_dict() for i in range(1, 21)]

    with open("questions.json", "w+", encoding='utf-8') as f:
        json.dump(liste_questions, f, ensure_ascii=False)

    return


def test():
    d = get_random_date()
    t = get_timestamp(d)
    print(d)
    print(t)


if __name__ == "__main__":
    stocker_questions()
