const padNombre = (n: number): string => {
  return n >= 10 ? n.toString(10) : `0${n}`;
};

export enum Sujet {
  Achats = 'Achats',
  Argent = 'Argent',
  Automobile = 'Automobile',
  Cuisine = 'Cuisine',
  Menage = 'Ménage',
  Sante = 'Santé',
  Travail = 'Travail',
  Autre = 'Autre',
}

export enum Tri {
  Ancien = 'Ancien à récent',
  Recent = 'Récent à ancien',
  Votes = 'Les mieux votées',
  PlusCommentees = 'Les plus commentées',
  MoinsCommentees = 'Les moins commentées',
}

export class Question {
  titre: string;
  texte: string;
  idQuestion: number;
  idAuteur: number;
  datePublication: Date;
  sujet: Sujet;
  upvotes: number;
  downvotes: number;
  nbCommentaires: number;

  constructor(
    titre: string,
    texte: string,
    idQuestion: number,
    idAuteur: number,
    timestamp: number,
    sujet: Sujet,
    upvotes: number,
    downvotes: number,
    nbCommentaires: number
  ) {
    this.titre = titre;
    this.texte = texte;
    this.idQuestion = idQuestion;
    this.idAuteur = idAuteur;
    this.datePublication = new Date(timestamp * 1000);
    this.sujet = sujet;
    this.upvotes = upvotes;
    this.downvotes = downvotes;
    this.nbCommentaires = nbCommentaires;
  }

  compareDate(otherQuestion: Question, chronological: boolean): number {
    if (
      this.datePublication.getTime() === otherQuestion.datePublication.getTime()
    ) {
      return 0;
    }

    const returnValue: number =
      this.datePublication < otherQuestion.datePublication ? -1 : 1;

    return chronological ? returnValue : -1 * returnValue;
  }

  compareVotes(otherQuestion: Question): number {
    const thisVotes: number = this.upvotes - this.downvotes;
    const otherVotes: number = otherQuestion.upvotes - otherQuestion.downvotes;

    if (thisVotes === otherVotes) {
      return this.compareDate(otherQuestion, true);
    }

    return thisVotes > otherVotes ? -1 : 1;
  }

  compareCommentaires(otherQuestion: Question, ascending: boolean): number {
    if (this.nbCommentaires === otherQuestion.nbCommentaires) {
      return this.compareDate(otherQuestion, true);
    }

    const returnValue: number =
      this.nbCommentaires < otherQuestion.nbCommentaires ? -1 : 1;

    return ascending ? returnValue : returnValue * -1;
  }

  getVoteDelta(): string {
    const delta: number = this.upvotes - this.downvotes;
    if (delta === 0) {
      return '0';
    }

    const sign: string = delta > 0 ? '+' : '-';

    return sign + delta.toString(10);
  }

  getIcon(): string {
    let icon: string;

    switch (this.sujet) {
      case Sujet.Achats:
        icon = 'mdi-shopping';
        break;

      case Sujet.Argent:
        icon = 'mdi-currency-eur';
        break;

      case Sujet.Automobile:
        icon = 'mdi-car';
        break;

      case Sujet.Cuisine:
        icon = 'mdi-chef-hat';
        break;

      case Sujet.Menage:
        icon = 'mdi-spray-bottle';
        break;

      case Sujet.Sante:
        icon = 'mdi-bottle-tonic-plus';
        break;

      case Sujet.Travail:
        icon = 'mdi-briefcase';
        break;

      default:
        icon = 'mdi-help-circle';
        break;
    }
    return icon;
  }

  getLink(): string {
    return `https://localhost:3000/post/${this.idQuestion}`;
  }

  getDateStr(): string {
    const listeMois: string[] = [
      'Jan',
      'Fev',
      'Mar',
      'Avr',
      'Mai',
      'Jun',
      'Jul',
      'Aug',
      'Sep',
      'Oct',
      'Nov',
      'Dec',
    ];

    const jour: string = padNombre(this.datePublication.getDate());

    const indexMois: number = this.datePublication.getMonth();
    const mois: string = listeMois[indexMois];
    const moisStr = mois !== 'Mai' ? mois + '.' : mois;

    const annee: number = this.datePublication.getFullYear();

    const heures: string = padNombre(this.datePublication.getHours());
    const minutes: string = padNombre(this.datePublication.getMinutes());

    return `${jour} ${moisStr} ${annee} - ${heures}:${minutes}`;
  }
}

export const sortQuestions = (
  a: Question,
  b: Question,
  methodeTri: Tri
): number => {
  let returnValue: number;

  switch (methodeTri) {
    case Tri.Ancien:
      returnValue = a.compareDate(b, true);
      break;

    case Tri.Recent:
      returnValue = a.compareDate(b, false);
      break;

    case Tri.Votes:
      returnValue = a.compareVotes(b);
      break;

    case Tri.PlusCommentees:
      returnValue = a.compareCommentaires(b, false);
      break;

    case Tri.MoinsCommentees:
      returnValue = a.compareCommentaires(b, true);
      break;

    default:
      console.error(`Méthode de tri inconnue: ${methodeTri}`);
      returnValue = 0;
      break;
  }

  return returnValue;
};
